const startButton = document.getElementById("startButton");

const imgArr = [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8];

function shuffleArray(array) {
  for (let index = array.length - 1; index > 0; index--) {
    const randomIndex = Math.floor(Math.random() * (index + 1));
    [array[index], array[randomIndex]] = [array[randomIndex], array[index]];
  }
}

shuffleArray(imgArr);

// card container

// start of game
let gameStart = false;

startButton.addEventListener("click", () => {
  gameStart = true;
  startTimer();
  startButton.style.color = "grey";
  // startButton.removeEventListener('click');
});

const cardContainer = document.getElementById("card-container");

imgArr.forEach((element) => {
  // Create the main card element
  const card = document.createElement("div");
  card.classList.add("card");

  // Create the front view element
  const frontView = document.createElement("div");
  frontView.classList.add("view", "front-view");

  // Create the icon image element for front view
  const frontImage = document.createElement("img");
  frontImage.src = "./images/black_tile.png";
  frontImage.alt = "icon";
  frontView.appendChild(frontImage);

  // Create the back view element
  const backView = document.createElement("div");
  backView.classList.add("view", "back-view");

  // Create the card image element for back view
  const backImage = document.createElement("img");
  backImage.src = `./images/4x4/img_${element}.png`;
  backImage.alt = "card-img";
  backView.appendChild(backImage);

  // Add front view and back view to the card
  card.appendChild(frontView);
  card.appendChild(backView);

  // Add the card to the cardContainer element
  cardContainer.appendChild(card);
});

//moves variable

let moveContent = document.getElementById("moves");
let move = 0;

// click card

const cards = document.querySelectorAll(".card");

let matched = 0;
let cardOne, cardTwo;
let disableDeck = false;

function flipCard({ target: clickedCard }) {

    console.log(clickedCard);
  if (cardOne !== clickedCard && !disableDeck) {
    clickedCard.classList.add("flip");

    ++move;
    moveContent.textContent = `${move} Moves`;

    if (move === 1 || gameStart === true) {
      // console.log("inside timer");
      startTimer();
    }

    if (!cardOne) {
      return (cardOne = clickedCard);
    }
    cardTwo = clickedCard;
    disableDeck = true;
    let cardOneImg = cardOne.querySelector(".back-view img").src,
      cardTwoImg = cardTwo.querySelector(".back-view img").src;
    matchCards(cardOneImg, cardTwoImg);
  }
}

function matchCards(cardOneImg, cardTwoImg) {
  if (cardOneImg === cardTwoImg) {
    setTimeout(() => {
      delayFunction();
    }, 1000);

    function delayFunction() {
      ++matched;
      cardOne.classList.add("opacity");
      // console.log("inside if");
      cardTwo.classList.add("opacity");

      cardOne.removeEventListener("click", flipCard);
      cardTwo.removeEventListener("click", flipCard);

      if (matched == 8) {
        return resetGame();
      }
    }
  }

  setTimeout(() => {
    cardOne.classList.remove("flip");
    cardTwo.classList.remove("flip");
    disableDeck = false;
    cardOne = cardTwo = "";
  }, 1000);

  //
}

// cards.forEach((card) => {
//   card.addEventListener("click", flipCard);
//   // return;
// });

cardContainer.addEventListener('click',flipCard);

//Reset Game

function resetGame() {
  moveContent.textContent = "0 Moves";
  move = 0;

  disableDeck = false;

  matched = 0;

  shuffleArray(imgArr);

  resetTimer();

  gameStart = false;

  cards.forEach((card, index) => {
    card.classList.remove("flip", "opacity");

    let cardImage = card.querySelector(".back-view img");
    cardImage.src = `./images/4x4/img_${imgArr[index]}.png`;

    card.addEventListener("click", flipCard);
  });
}

// timer function
const timeContainer = document.getElementById("time");
let timeInSec = 0;
let interval;

function startTimer() {
  // Clear any previous interval to avoid multiple timers running simultaneously
  clearInterval(interval);

  interval = setInterval(() => {
    timer();
  }, 1000);
}

function stopTimer() {
  clearInterval(interval);
}

function resetTimer() {
  timeInSec = 0;
  timeContainer.textContent = `Time: ${timeInSec} Secs`;
}

function timer() {
  ++timeInSec;
  timeContainer.textContent = `Time: ${timeInSec} Secs`;
}


